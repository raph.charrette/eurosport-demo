module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        play1: "#1fb6ff",
        play2: "#A7171A",
        eurosport: "#161B49",
      },
    },
  },
  plugins: [],
};
