import React from "react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";

import PlayersWrapper from "./components/Wrapper";

const client = new ApolloClient({
  uri: "https://kf9p4bkih6.execute-api.eu-west-1.amazonaws.com/dev/",
  cache: new InMemoryCache(),
});

function App() {
  return (
    <ApolloProvider client={client}>
      <PlayersWrapper />
    </ApolloProvider>
  );
}

export default App;
