import React from "react";

import { PlayerT } from "../../types";
import Player from "../Player";

interface PlayersWrapperProps {
  players?: PlayerT[];
}

const PlayersWrapper: React.FC<PlayersWrapperProps> = ({ players }) => {
  return (
    <div className="flex justify-around gap-12">
      {players?.map((player, i) => (
        <Player key={i} {...player} />
      ))}
    </div>
  );
};

export default PlayersWrapper;
