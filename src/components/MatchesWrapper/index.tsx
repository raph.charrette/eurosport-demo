import React, { useState } from "react";
import moment from "moment";

import { MatchT } from "../../types";
import { toFormat } from "../../utils/date";

interface MatchesProps {
  matches?: MatchT[];
  p1winn: number;
}

const MatchesWrapper: React.FC<MatchesProps> = ({ p1winn, matches }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <div className="text-center my-12 text-5xl">
        <span className="text-play1">{p1winn}</span> /{" "}
        <span className="text-play2">{(matches?.length || 0) - p1winn}</span>
      </div>
      <div
        onClick={() => setIsOpen((e) => !e)}
        className="cursor-pointer underline font-bold text-eurosport hover:text-blue-900 text-center"
      >
        {!isOpen ? "Click to see more" : "Click to see less"}
      </div>
      {isOpen && (
        <div className="relative">
          <div className="absolute left-1/2 top-0 my-12">
            {matches &&
              [...matches]
                ?.sort((a, b) => {
                  return (
                    new Date(b.startTime).getTime() -
                    new Date(a.startTime).getTime()
                  );
                })
                ?.map(({ startTime, endTime, winner: { id } }, i) => {
                  const duration = moment.duration(
                    moment(endTime).diff(startTime)
                  );
                  const winn1 = id === "player-1";

                  return (
                    <div
                      key={i}
                      className={`${
                        winn1
                          ? "bg-play1/20 rounded-tl-lg rounded-bl-lg right-0 justify-start"
                          : "bg-play2/20 rounded-tr-lg rounded-br-lg left-0 justify-end"
                      }
                  absolute
                  h-[30px]
                  px-4
                  py-2
                  flex
                  gap-4
                  items-center
                  w-[300px]
                `}
                      style={{ top: `${i * 40}px` }}
                    >
                      <div
                        className={`${
                          winn1 ? "order-0" : "order-1"
                        } font-bold text-xl`}
                      >
                        {toFormat(new Date(startTime))}
                      </div>
                      <div
                        className={`${winn1 ? "order-1" : "order-0"} text-sm`}
                      >{`(${duration.hours()}h${duration.minutes()}min)`}</div>
                    </div>
                  );
                })}
          </div>
        </div>
      )}
    </div>
  );
};

export default MatchesWrapper;
