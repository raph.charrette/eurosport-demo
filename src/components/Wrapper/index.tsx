import React, { useEffect, useState } from "react";
import { gql, useQuery } from "@apollo/client";

import LogoImg from "../../assets/logo-europsport.jpeg";

import Loader from "../Loader";
import PlayersWrapper from "../PlayersWrapper";
import MatchesWrapper from "../MatchesWrapper";

import { MatchT, PlayerT } from "../../types";

const GET_PLAYERS_INFO = gql`
  query {
    players {
      firstname
      lastname
      picture {
        url
      }
      id
      sex
      country {
        picture {
          url
        }
        code
      }
      stats {
        points
        rank
        age
        weight
        height
      }
    }
    matches {
      id
      startTime
      endTime
      winner {
        id
      }
    }
  }
`;

const Wrapper: React.FC = () => {
  const { data, loading } = useQuery<{ players: PlayerT[]; matches: MatchT[] }>(
    GET_PLAYERS_INFO
  );

  const [winnP1, setWinnP1] = useState(0);

  useEffect(() => {
    setWinnP1(
      data?.matches?.reduce((acc, { winner: { id } }) => {
        return id === "player-1" ? acc + 1 : acc;
      }, 0) || 0
    );
  }, [data]);

  return (
    <div className="w-[80%] m-auto">
      <div className="my-12 flex items-center flex-col">
        <img src={LogoImg} className="w-[200px]" alt="logo" />
      </div>
      {loading ? (
        <Loader />
      ) : (
        <div>
          <PlayersWrapper players={data?.players} />
          <MatchesWrapper p1winn={winnP1} matches={data?.matches} />
        </div>
      )}
    </div>
  );
};

export default Wrapper;
