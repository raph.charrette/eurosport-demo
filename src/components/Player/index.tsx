import React from "react";

import { PlayerT } from "../../types";

const unitClasses = "text-sm font-normal";

const Player: React.FC<PlayerT> = ({
  id,
  firstname,
  lastname,
  country: {
    code,
    picture: { url: countryPic },
  },
  picture: { url: playerPic },
  stats: { points, rank, age, height, weight },
}) => {
  return (
    <div
      className={`border rounded-lg py-6 px-8 w-1/3 relative shadow-lg ${
        id === "player-1" ? "bg-play1/20" : "bg-play2/20"
      }`}
    >
      <img
        src={playerPic}
        alt={`${id}-pic`}
        className={
          "w-[82px] h-[82px] object-cover rounded-full absolute top-[-41px] left-1/2 -translate-x-1/2 shadow-lg"
        }
      />
      <img
        src={countryPic}
        className="absolute top-[10px] left-1/2 translate-x-[20px] w-[30px] h-[20px] shadow-xl"
        alt={`${id}-country`}
      />
      <div className="text-center mt-10">
        <div className="text-3xl font-bold">{`${firstname} ${lastname}`}</div>
        <div className="italic font-light text-sm">#{rank}</div>
      </div>
      <div className="mt-6 text-center">
        <div className="bg-white rounded-lg px-6 py-2">
          <div className="font-bold">
            <span className="text-xl">{points}</span> points
          </div>
        </div>
        <div className="text-right flex justify-around mt-6">
          <div className="font-bold">
            {age} <span className={unitClasses}>ans</span>
          </div>
          <div className="font-bold">
            {height} <span className={unitClasses}>cm</span>
          </div>
          <div className="font-bold">
            {weight / 1000} <span className={unitClasses}>kg</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Player;
