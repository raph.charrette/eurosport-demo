import moment from "moment";

export const toFormat = (date: Date) => moment(date).format("D/MM/YYYY");
