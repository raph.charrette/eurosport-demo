export type PlayerT = {
  id: string;
  firstname: string;
  lastname: string;
  picture: {
    url: string;
  };
  sex: string;
  country: {
    picture: {
      url: string;
    };
    code: string;
  };
  stats: {
    points: number;
    rank: number;
    age: number;
    weight: number;
    height: number;
  };
};

export type MatchT = {
  id: string;
  startTime: string;
  endTime: string;
  winner: {
    id: string;
  };
};
